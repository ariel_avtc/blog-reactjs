import React from 'react';

const App = () => {
    return (
        <div>
            <h1>Hello world from ReactJS!</h1>
        </div>
    );
};

export default App;