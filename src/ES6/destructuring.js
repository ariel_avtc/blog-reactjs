const destructuringArray = () => {
    let { num1, num2 } = [1, 2];
    console.log(num1 + num2); // 3

    let { num1, ...nums } = [1, 2, 3, 4, 5];
    console.log(nums); // [2,3,4,5]
}

const destructuringObject = () => {
        let person = {
            name: 'Ariel',
            lastName: 'Gonzales'
        };

        let { name, lastName } = person;

        console.log(name + ' ' + lastName); // Ariel Gonzales
}