class Calculator {
    sum(num1, num2) {
        return num1 + num2;
    }

    substract(num1, num2) {
        return num1 - num2;
    }

    multiply(num1, num2) {
        return num1 * num2;
    }

    divide(num1, num2) {
        return num1 / num2;
    }
}

class ExtendedCalculator extends Calculator {
    static modulus(num1, num2) {
        return num1 % num2;
    }
}

export {
    Calculator,
    ExtendedCalculator
};