let arrayNames = ['Hugo', 'Paco', 'Luis'];
let arrayFruits = ['Apple', 'Banana', 'Grape'];
let arrayMonths = ['January', 'February', 'March'];
let arrayNumbers = [1, 2, 3, 5, 8, 13];

function manageByIndex() {
    let firstFruit = arrayFruits[0];
    console.log(firstFruit); // Apple

    console.log(arrayFruits[1]); //Banana
    arrayFruits[1] = 'Avocado';
    console.log(arrayFruits[1]); //Avocado

    console.log(arrayFruits.indexOf('Grape')); //2
}

function createModifyArray() {
    console.log(arrayNames); //["Hugo","Paco","Luis"]
    arrayNames.push('Ariel', 'Gael');
    console.log(arrayNames); //["Hugo","Paco","Luis","Ariel","Gael"]
    arrayNames.pop();
    console.log(arrayNames); //["Hugo","Paco","Luis","Ariel"]

    let deletedItems = arrayMonths.splice(0, 2);
    console.log(arrayMonths); //["March"]
    console.log(deletedItems); //["January","February"]

    arrayMonths = deletedItems.concat(arrayMonths);
    console.log(arrayMonths); //["January","February", "March"]

    let newArray = arrayMonths.slice(2);
    console.log(arrayMonths); //["January","February", "March"]
    console.log(newArray); //["March"]
}

function iterateArray() {
    arrayMonths.forEach((element, index) => {
        console.log(element, index);
    })

    let newNumbersArray = arrayNumbers.map((value, index) => {
        return value * 2;
    });
    console.log(newNumbersArray); //[2, 4, 6, 10, 16, 26]

    newNumbersArray = arrayNumbers.filter((value, index) => {
        return value > 5;
    });
    console.log(newNumbersArray); //[8, 13]

    let sum = arrayNumbers.reduce((total, value, index) => {
        return total + value;
    });
    console.log(sum); // 32

    let result = arrayNumbers.every((value, index) => {
        return value < 20;
    });
    console.log(result); // true

    result = arrayNumbers.some((value, index) => {
        return value > 10;
    });
    console.log(result); // true

    result = arrayNumbers.find((value, index)=>{
        return value > 7;
    })
    console.log(result); // 8
}
