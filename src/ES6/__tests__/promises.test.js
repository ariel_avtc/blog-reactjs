import * as Promises from '../promises';

test('Promise test', () => {
    Promises.todoPromise()
        .then(result => {
            expect(result).toBe(`{ completed: false,
                        id: 1,
                        title: "delectus aut autem",
                        userId: 1
                      }`);
        })
        .catch(error => console.log(error))
});