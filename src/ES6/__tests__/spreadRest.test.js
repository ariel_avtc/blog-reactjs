import * as SpreadRest from '../spreadRest';

test('Spread operator test', () => {
    expect(SpreadRest.spreadOperator()).toEqual([1, 2, 3, 5, 8, 13, 21]);
});

test('Rest operator test', () => {
    expect(SpreadRest.restOperator(1, 2, 3, 5, 8, 13, 21)).toBe(53);
});