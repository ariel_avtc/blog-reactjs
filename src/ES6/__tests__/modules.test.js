import * as module from '../modules';

test('test sayHi', () => {
    expect(module.sayHi('Ariel')).toBe('Hello Ariel!');
});

test('test sayBye', () => {
    expect(module.sayBye('Ariel')).toBe('Bye Ariel!');
});