import * as EnhancedObjectLiterals from '../enhancedObjectLiterals';

test('Test object literals', () => {
    let car = EnhancedObjectLiterals.getCar('Sentra', 2016);
    expect(car).toHaveProperty('model', 'Sentra');
    expect(car).toHaveProperty('id1', 1);
})