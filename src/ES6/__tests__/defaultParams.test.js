import * as DefaultParams from '../defaultParams';

test('Test default params', () => {
    let car1 = DefaultParams.getCar('Versa', 2016);
    let car2 = DefaultParams.getCar('Versa');
    let car3 = DefaultParams.getCar();
    expect(car1).toHaveProperty('model', 'Versa');
    expect(car1).toHaveProperty('year', 2016);

    expect(car2).toHaveProperty('model', 'Versa');
    expect(car2).toHaveProperty('year', 1980);

    expect(car3).toHaveProperty('model', 'Corolla');
    expect(car3).toHaveProperty('year', 1980);
})