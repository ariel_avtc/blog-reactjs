import { Calculator, ExtendedCalculator } from '../classes';

const calc = new Calculator();

test('test Calculator.sum', () => {
    expect(calc.sum(1, 2)).toBe(3);
});

test('test Calculator.substract', () => {
    expect(calc.substract(9, 5)).toBe(4);
});

test('test Calculator.multiply', () => {
    expect(calc.multiply(3, 4)).toBe(12);
});

test('test Calculator.divide', () => {
    expect(calc.divide(20, 5)).toBe(4);
});

test('test ExtendedCalculator.modulus', () => {
    expect(ExtendedCalculator.modulus(5, 2)).toBe(1);
});