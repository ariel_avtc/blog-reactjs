import * as TemplateStrings from '../templateStrings';

test('Test template string',()=>{
    expect(TemplateStrings.getHello('Ariel')).toBe(`Returning a 
    Hello World 
    to Ariel`);
});