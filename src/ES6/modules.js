export const sayHi = (name) => {
    return 'Hello ' + name + '!';
}

export const sayBye = (name) => {
    return 'Bye ' + name + '!';
}