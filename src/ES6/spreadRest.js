export const spreadOperator = () => {
    let arrayNumbers1 = [1, 2, 3, 5];
    let arrayNumbers2 = [8, 13, 21];

    let fibonacci = [...arrayNumbers1, ...arrayNumbers2];

    return fibonacci; // [1,2,3,5,8,13,21]
}

export const restOperator = (...args) => {
    let total = args.reduce((total, value) => {
        return total + value;
    })

    return total;
}