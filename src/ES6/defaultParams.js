export const getCar = (model = 'Corolla', year = 1980) => {
    return {
        model,
        year
    }
}