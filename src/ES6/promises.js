import fetch from 'node-fetch';

export const todoPromise = () => {
    return new Promise((resolve, reject) => {
        try {
            fetch('https://jsonplaceholder.typicode.com/todos/1')
                .then(response => response.json())
                .then(json => resolve(json));
        } catch (error) {
            reject(error);
        }
    })
}

export async function todoPromiseAwait() {
    return new Promise((resolve, reject) => {
        try {
            fetch('https://jsonplaceholder.typicode.com/todos/1')
                .then(response => response.json())
                .then(json => resolve(json));
        } catch (error) {
            reject(error);
        }
    })
}

let resultAwait = await todoPromiseAwait();

console.log(resultAwait);